import os
import semanage
import selinux

sh = semanage.semanage_handle_create()
semanage.semanage_connect(sh)
semanage.semanage_set_reload(sh, 0)
semanage.semanage_set_check_contexts(sh, 0)
semanage.semanage_set_default_priority(sh, 500)
semanage.semanage_module_install_file(sh, "/etc/selinux.d/xyz.cil")
semanage.semanage_commit(sh)

os.system("cat /etc/selinux.d/xyz.cil")
print("after semanage.semanage_set_reload(sh, 0)")
os.system("sesearch -A -s sshd_t -t init_t -c file -p execute")
print()
selinux.selinux_mkload_policy(1)

print("after selinux.selinux_mkload_policy(1)")
os.system("sesearch -A -s sshd_t -t init_t -c file -p execute")
