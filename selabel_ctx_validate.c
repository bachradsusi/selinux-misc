#include <stdio.h>
#include <stdlib.h>

#include <selinux/selinux.h>
#include <selinux/label.h>

int main () {
    struct selinux_opt selinux_opts[] = {
    { SELABEL_OPT_VALIDATE, "1" },
    { SELABEL_OPT_PATH, "/tmp/file_contexts" },
};

    struct selabel_handle *selabel = selabel_open(SELABEL_CTX_FILE, selinux_opts, 2);
	if (!selabel) {
		perror("/tmp/file_contexts");
		exit(1);
	}

}
