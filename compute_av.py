#!/usr/bin/python3

import selinux
import sys

avd = selinux.av_decision()
class_file = selinux.string_to_security_class(sys.argv[3])
selinux.security_compute_av(sys.argv[1], sys.argv[2], class_file, 1, avd)
selinux.print_access_vector(class_file, avd.allowed)
