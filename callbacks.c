#define _GNU_SOURCE

#include <selinux/selinux.h>
#include <selinux/avc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>


int func_log(int type, const char *fmt, ...) {
  char *buf;
  va_list ap;
  int audit_seq;

  va_start(ap, fmt);
  vasprintf(&buf, fmt, ap);
  va_end(ap);
  printf("my_logger: %s\n", buf);
  free(buf);
  return 1;
}


int main() {
  union selinux_callback callback;

  selinux_status_open(0);

  selinux_status_updated();
  selinux_mkload_policy(0);
  selinux_status_updated();

  selinux_set_callback(SELINUX_CB_LOG, (union selinux_callback) &func_log);
  selinux_mkload_policy(0);
  selinux_status_updated();


}
