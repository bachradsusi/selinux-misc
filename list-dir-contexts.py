#!/usr/bin/python3

import selinux
import semanage
import sys

# ./list-dir-contexts.py /etc/httpd 
# httpd_config_t
# cert_t
# ipa_cert_t
# httpd_log_t
# httpd_modules_t
# httpd_keytab_t
# httpd_config_t


directory = sys.argv[1]
directory_len = (len(directory))

handle = semanage.semanage_handle_create()
semanage.semanage_connect(handle)

(rc, fclist) = semanage.semanage_fcontext_list(handle)
(rc, fclocal) = semanage.semanage_fcontext_list_local(handle)
(rc, fchome) = semanage.semanage_fcontext_list_homedirs(handle)

for fcontext in fclist + fclocal + fchome:
    expression = semanage.semanage_fcontext_get_expr(fcontext)
    if expression[0:directory_len] == directory:
        con = semanage.semanage_fcontext_get_con(fcontext)
        if con:
            print(semanage.semanage_context_get_type(con))

selabel = selinux.selabel_open(selinux.SELABEL_CTX_FILE, None, 0)
(rc, context) = selinux.selabel_lookup(selabel, directory, 0)
print(context.split(':')[2])
