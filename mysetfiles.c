#include <selinux/restorecon.h>
#include <selinux/selinux.h>
#include <selinux/label.h>

int main(int argc, char **argv) {
    struct selinux_opt selinux_opts[] = {
        { SELABEL_OPT_VALIDATE, (char *)1 },
        { SELABEL_OPT_PATH, "/sysroot/etc/selinux/targeted/contexts/files/file_contexts" },
        { SELABEL_OPT_DIGEST, (char *)1 }
    };
    unsigned int flags = SELINUX_RESTORECON_RECURSE |
        SELINUX_RESTORECON_LOG_MATCHES | SELINUX_RESTORECON_VERBOSE;
    struct selabel_handle *hndl;
    char *exclude_list[] = {
      "/sysroot/dev", "/sysroot/proc", "/sysroot/selinux", "/sysroot/sys", 0
    };
    int rc;

    hndl = selabel_open(SELABEL_CTX_FILE, selinux_opts, 3);

    selinux_restorecon_set_sehandle(hndl);
    selinux_restorecon_set_alt_rootpath("/sysroot");
    selinux_restorecon_set_exclude_list((const char **)exclude_list);
    rc = selinux_restorecon("/sysroot", flags);
    return (rc == -1 ? 1 : 0);
}
